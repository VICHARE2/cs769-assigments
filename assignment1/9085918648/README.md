# Additional instructions before executing main.py

Instead of a dedicated `setup.py` script, the pre-fetching steps are added in `run_exp.sh` script itself.
Please find the `run_exp.sh` file which downloads the pre-trained Fasttext word embeddings.

These embeddings can also be downloaded separately via [this link](https://dl.fbaipublicfiles.com/fasttext/vectors-english/crawl-300d-2M.vec.zip). These will need to be extracted and the path of the `crawl-300d-2M.vec` file needs to be supplied something like thisL `--emb_file="./crawl-300d-2M.vec"`.

Rest of the results and reports can be found in `report.pdf`
