import torch
import torch.nn as nn
import zipfile
import numpy as np


class BaseModel(nn.Module):
    def __init__(self, args, vocab, tag_size):
        super(BaseModel, self).__init__()
        self.args = args
        self.vocab = vocab
        self.tag_size = tag_size

    def save(self, path):
        # Save model
        print(f"Saving model to {path}")
        ckpt = {"args": self.args, "vocab": self.vocab, "state_dict": self.state_dict()}
        torch.save(ckpt, path)

    def load(self, path):
        # Load model
        print(f"Loading model from {path}")
        ckpt = torch.load(path)
        self.vocab = ckpt["vocab"]
        self.args = ckpt["args"]
        self.load_state_dict(ckpt["state_dict"])


def load_embedding(vocab, emb_file, emb_size):
    """
    Read embeddings for words in the vocabulary from the emb_file (e.g., GloVe, FastText).
    Args:
        vocab: (Vocab), a word vocabulary
        emb_file: (string), the path to the embdding file for loading
        emb_size: (int), the embedding size (e.g., 300, 100) depending on emb_file
    Return:
        emb: (list), list of embedding/None of size (|vocab|, )
    """
    with open(emb_file, "r", encoding="utf-8", errors="ignore", newline="\n") as vfile:
        n_tokens, dim_size = map(int, vfile.readline().split())
        assert (emb_size, dim_size)
        data = [None] * len(vocab)
        for i, line in enumerate(vfile):
            word, *vec = line.rstrip().split(" ")
            if word.lower() in vocab:
                word_idx = vocab[word.lower()]
                data[word_idx] = list(map(float, vec))
    return data


class DanModel(BaseModel):
    def __init__(self, args, vocab, tag_size):
        super(DanModel, self).__init__(args, vocab, tag_size)
        self.define_model_parameters()
        self.init_model_parameters()

        # Use pre-trained word embeddings if emb_file exists
        if args.emb_file is not None:
            self.copy_embedding_from_numpy()

    def define_model_parameters(self):
        """
        Define the model's parameters, e.g., embedding layer, feedforward layer.
        Pass hyperparameters explicitly or use self.args to access the hyperparameters.
        """
        # Defining Embedding layer
        self.embeddings = nn.Embedding(
            len(self.vocab),
            self.args.emb_size,
            padding_idx=self.vocab.pad_id,
            _freeze=False,
        )
        if self.args.enable_batch_norm is "true":
            self.batch_norm = nn.BatchNorm1d(
                self.args.emb_size, affine=False, track_running_stats=False
            )
        else:
            self.batch_norm = None

        self.emb_drop = nn.Dropout(self.args.emb_drop)

        # Defining Classifier
        # # Determining nonlinearity
        if self.args.non_linearity is "CELU":
            nonlinearity = nn.CELU(alpha=5.0)
        elif self.args.non_linearity is "LeakyReLU":
            nonlinearity = nn.LeakyReLU()
        else:
            nonlinearity = nn.ReLU()
        # # Defining layers dimensions
        layers = []
        layer_dims = (
            [self.args.emb_size]
            + [self.args.hid_size, self.args.hid_size] * self.args.hid_layer
            + [self.tag_size]
        )
        # # Add layers to classifier layer Ref: https://docs.python.org/2/library/functions.html#zip
        for inp_size, out_size in zip(*[iter(layer_dims)] * 2):
            layers.append(nn.Linear(inp_size, out_size))
            layers.append(nonlinearity)
            layers.append(nn.Dropout(self.args.hid_drop))
        layers.pop()  # Pop dropout
        layers.pop()  # Popping the extra nonlinearity layer
        self.classifier = nn.Sequential(*layers)

        self.softmax = nn.Softmax(dim=1)

    def init_model_parameters(self):
        """
        Initialize the model's parameters by uniform sampling from a range [-v, v], e.g., v=0.08
        Pass hyperparameters explicitly or use self.args to access the hyperparameters.
        """
        # Distribution follows the distribution of pre-trained weights of fasttext
        nn.init.trunc_normal_(
            self.embeddings.weight, mean=0.0, std=12**-0.5, a=-1.0, b=1.0
        )
        # Initializing Linear layers with Xavier/Glorot initialization
        for layer in self.classifier.children():
            if isinstance(layer, nn.Linear):
                nn.init.xavier_uniform_(layer.weight)
                nn.init.uniform_(layer.bias, -0.1, 0.1)

    def copy_embedding_from_numpy(self):
        """
        Load pre-trained word embeddings from list of embeddings to nn.embedding
        Pass hyperparameters explicitly or use self.args to access the hyperparameters.
        """
        # Read embedding file
        pt_embeddings = load_embedding(
            self.vocab, self.args.emb_file, self.args.emb_size
        )
        # Selectively update weights with no_grad
        with torch.no_grad():
            for i, pt_embed in enumerate(pt_embeddings):
                if pt_embed is not None:
                    self.embeddings.weight[i] = torch.Tensor(pt_embed)

    def forward(self, x):
        """
        Compute the unnormalized scores for P(Y|X) before the softmax function.
        E.g., feature: h = f(x)
              scores: scores = w * h + b
              P(Y|X) = softmax(scores)
        Args:
            x: (torch.LongTensor), [batch_size, seq_length]
        Return:
            scores: (torch.FloatTensor), [batch_size, ntags]
        """
        # Counts of non-pad words
        word_counts = torch.sum(x != self.vocab.pad_id, dim=1)
        # Get word embeddings
        x = self.embeddings(x)
        # Compute sentence embedding
        embs = torch.sum(x, dim=1)
        x = torch.div(embs, torch.unsqueeze(word_counts, dim=1))
        if self.batch_norm is not None:
            x = self.batch_norm(x)
        x = self.emb_drop(x)
        # Pass
        x = self.classifier(x)
        # Getting class probabilities through softmax
        x = self.softmax(x)

        return x
