# CS 769 - Homework 1

## Student Details

-   **Name**: Shantanu Vichare
-   **ID**: 9085918648

## Improvements/Experiments

For my DAN model, I have incorporated the following capabilities:

-   Incorporated pre-trained FastText embeddings - 2 million word vectors trained on Common Crawl (600B tokens)
-   CELU non-linear activation for the hidden layers with alpha=5
-   Configurable Embedding Dropout and Hidden layer Dropout
-   Batch Normalization for batch of sentences
-   Configurable early stopping
-   Xavier (Glorot) initialization for classifier weights initialization
-   Fine-tuning of FastText embeddings and normal distributed embedding matrices for words without pre-trained embeddings
-   Custom pre-processing of corpus - compound words like _'co-writer/director'_ or _'cliché-laden'_ were getting categorized as independent words, thus unable to leverage the pre-trained embeddings. Adding a regex-based pre-processor helped in separating the words into 'co-writer', 'director', 'cliché', etc. allowing to use the pre-trained embeddings for such use cases

## Results

Following is a summary of the accuracy achieved on the SST and CFIMDB datasets. Each row lists down testing results for a comparative analysis of each feature/experiment.

Note: Base variant uses no pre-trained vectors, uses ReLU activation, no dropout, no batch normalization, default pre-processing

| Feature/Experiment                                          | File prefix                 | Dataset | dev accuracy | test accuracy |
| ----------------------------------------------------------- | --------------------------- | ------- | ------------ | ------------- |
| Base                                                        | base-sst-                   | SST     | 0.3606       | 0.3507        |
| Embedding                                                   | emb-sst-                    | SST     | 0.3760       | 0.3792        |
| CELU Activation + Embedding                                 | celu-emb-sst-               | SST     | 0.3751       | 0.3683        |
| Dropout + Embedding                                         | drop-emb-sst-               | SST     | 0.3815       | 0.3729        |
| Dropout + CELU + Embedding                                  | drop-celu-emb-sst-          | SST     | 0.3869       | 0.3814        |
| BatchNorm + Dropout + CELU + Embedding                      | batch-drop-celu-emb-sst-    | SST     | 0.3824       | 0.3873        |
| Custom Pre-process + BatchNorm + Dropout + CELU + Embedding | cp-batch-drop-celu-emb-sst- | SST     | 0.3833       | 0.3828        |
| Custom Pre-process + Dropout + CELU + Embedding             | cp-drop-celu-emb-sst-       | SST     | 0.3869       | 0.3733        |
| Custom Pre-process + Dropout + Embedding                    | cp-drop-emb-sst-            | SST     | 0.3833       | 0.3846        |
| Custom Pre-process + CELU + Embedding                       | cp-celu-emb-sst-            | SST     | 0.3915\*     | 0.3959\*      |
| Custom Pre-process + Embedding                              | cp-emb-sst-                 | SST     | 0.3860       | 0.3873        |
| Dropout + CELU + Embedding                                  | drop-celu-emb-cfimdb-       | CFIMDB  | 0.9184       | NA            |
| Custom Pre-process + Dropout + CELU + Embedding             | cp-drop-celu-emb-cfimdb-    | CFIMDB  | 0.9184       | NA            |
| Custom Pre-process + CELU + Embedding                       | cp-celu-emb-cfimdb-         | CFIMDB  | 0.9306\*     | NA            |

These files are available in OneDrive here: [https://uwprod-my.sharepoint.com/:f:/g/personal/vichare2_wisc_edu/EmVHeZtrz25EpZDpcd3UNJABoUo0iWjfhF_AIoFu9OK1iA](https://uwprod-my.sharepoint.com/:f:/g/personal/vichare2_wisc_edu/EmVHeZtrz25EpZDpcd3UNJABoUo0iWjfhF_AIoFu9OK1iA)

The files are structured as `experiments/{prefix}{filename}` where `filename` is one of {`model.pt`, `dev-output.txt`, `test-output.txt`}
