# CS 769 - Homework 2

## Student Details

- **Name**: Shantanu Vichare
- **ID**: 9085918648

## General information

1. Due to the long training times, I limited my training with the `pretrain` option to 50 epochs and 10 epochs for the `sst` and `cfimdb` datasets respectively. From the log file, it is apparent that the model is still underfitted and hence the results are sub-par. I'd advise to run for a lot more epochs if this functionality needs to be tested reliably. Despite this, I have included the results in my submission. Please find the below-mentioned files:

- `cfimdb-dev-output-pre.txt`
- `cfimdb-test-output-pre.txt`
- `cfimdb-train-pre-log.txt`
- `sst-dev-output-pre.txt`
- `sst-test-output-pre.txt`
- `sst-train-pre-log.txt`

2. My optimizer test was failing during my testing but the results are extremely close. So I request you to check my implementation and/or inspect the values of `ref` and `actual` arrays from your test script.

3. I was facing an installation failure in Google Colab for `tokenizers` with `v0.10.1` and I was able to make it work with `v0.13.2` instead. I have included my `setup.sh` in my submission for your reference.

## Results

Following is a summary of the accuracy achieved on the SST and CFIMDB datasets.

| Dataset | Option   | dev accuracy | test accuracy | epochs-status           |
| ------- | -------- | ------------ | ------------- | ----------------------- |
| SST     | finetune | 0.520        | 0.545         | 10 epochs               |
| SST     | pretrain | 0.336        | 0.328         | 50 epochs (Underfitted) |
| CFIMDB  | finetune | 0.967        | NA            | 10 epochs               |
| CFIMDB  | pretrain | 0.584        | NA            | 10 epochs (Underfitted) |
